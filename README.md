Projeto Java com Spring Boot, uma api REST com CRUD de Cliente (id, nome, cpf, dataNascimento). O CRUD com uma api de GET, POST, DELETE, PATCH e PUT.
 
Backend: Java / Spring Boot
Infraestrutura: PostgreSQL

O banco de dados estar em uma imagem Docker.

Instalação
Pré-requisitos de instalação

DOCKER, JAVA, MAVEN.
Por Favor, Seguir a ordem de instalação.

Passo 1 (PostgreSQL - Porta:5436):

1 - Abrir o diretório "ProjetoJavaSpringBoot/" via linha de comando;
2 - Executar o comando docker-compose up;
3 - Acessar a url, caso queira acesso ao banco de dados via pgadmin4, http://localhost:5050 Acesso: EMAIL: pgadmin4@pgadmin.org SENHA: admin

Passo 2 (Rodando a aplicação)

1 - Rodar o comando, via terminal, mvn install;
2 - Logo depois o comando: mvn spring-boot:run

Passo 3 (Executando os comandos GET, POST, DELETE, PATCH e PUT)

1 - Para apreciação da API, o instalador do Postman encontra-se dentro do diretório /Postman;

Exemplos:

GET - http://localhost:8080/clientes

#Get por CPF e NOME
http://localhost:8080/clientes/page?nome=Ma&cpf=96258785632

#Get por cpf
http://localhost:8080/clientes/porCpf?cpf=96258785632

#Get por nome
http://localhost:8080/clientes/porNome?nome=Ma

POST - http://localhost:8080/clientes
	    {
			"nome": "Ribamar Souza",
			"cpf": "96258785632",
			"dataNascimento": "14/07/1989"
    	}
		
PUT - http://localhost:8080/clientes/4 		
		 {
			"id": 4,
			"nome": "Julianna Paz Oliveira",
			"cpf": "965478525865",
			"dataNascimento": "24/07/1989"
    	}
		
PATCH - htttp://localhost:8080/clientes/4
	  	{
			"id": 4,
			"nome": "Julianna",
			"cpf": "965478525865",
			"dataNascimento": "24/07/1989"
    	}
		
DELETE - http://localhost:8080/clientes/4