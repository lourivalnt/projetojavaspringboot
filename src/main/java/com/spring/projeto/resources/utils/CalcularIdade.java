package com.spring.projeto.resources.utils;

import java.time.LocalDate;
import java.time.Period;

import com.spring.projeto.domain.Cliente;

public class CalcularIdade {
	
	public static long idade(Cliente cliente) {
		return Period.between(cliente.getDataNascimento(), LocalDate.now()).getYears();
	}
}
