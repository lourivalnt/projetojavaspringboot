package com.spring.projeto;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.spring.projeto.domain.Cliente;
import com.spring.projeto.repositories.ClienteRepository;

@SpringBootApplication
public class ProjetoJavaSpringBootApplication implements CommandLineRunner {

	@Autowired
	private ClienteRepository clienteRepository;

	public static void main(String[] args) {
		SpringApplication.run(ProjetoJavaSpringBootApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("20/06/1980");
		
        LocalDate hoje = LocalDate.now();
        LocalDate outraData1 = LocalDate.of(1988, Month.SEPTEMBER, 26);
        LocalDate outraData2 = LocalDate.of(1985, Month.APRIL, 15);
        LocalDate outraData3 = LocalDate.of(1978, Month.AUGUST, 10);
        LocalDate outraData4 = LocalDate.of(1987, Month.JANUARY, 05);
        LocalDate outraData5 = LocalDate.of(1990, Month.MARCH, 14);
        LocalDate outraData6 = LocalDate.of(1983, Month.JUNE, 06);
        
        
        
        long idade1 = Period.between(outraData1, LocalDate.now()).getYears();
        long idade2 = Period.between(outraData2, LocalDate.now()).getYears();
        long idade3 = Period.between(outraData3, LocalDate.now()).getYears();
        long idade4 = Period.between(outraData4, LocalDate.now()).getYears();
        long idade5 = Period.between(outraData5, LocalDate.now()).getYears();
        long idade6 = Period.between(outraData6, LocalDate.now()).getYears();
        
		Cliente cli1 = new Cliente(null, "Maria Silva", "96258785632", outraData1, idade1);
		Cliente cli2 = new Cliente(null, "Josiel Morais", "74185296332", outraData2, idade2);
		Cliente cli3 = new Cliente(null, "Darlan Adriano", "98745632120", outraData3, idade3);
		Cliente cli4 = new Cliente(null, "Julianna Paz", "965478525865", outraData4, idade4);
		Cliente cli5 = new Cliente(null, "Thiego Ramos", "32145698752", outraData5, idade5);
		Cliente cli6 = new Cliente(null, "Ana Maria", "85274196332", outraData6, idade6);
	
		clienteRepository.saveAll(Arrays.asList(cli1,cli2,cli3,cli4,cli5,cli6));
	}
}
