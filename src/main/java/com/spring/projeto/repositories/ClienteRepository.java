package com.spring.projeto.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.projeto.domain.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	public Page<Cliente> findByNomeIgnoreCaseContaining(String nome, Pageable pageable);
	
	public Page<Cliente> findByCpf(String cpf, Pageable pageable);
	
	@Transactional(readOnly=true)
	@Query("SELECT DISTINCT obj FROM Cliente obj WHERE obj.nome LIKE %:nome% OR obj.cpf LIKE %:cpf%")
	Page<Cliente> search(@Param("nome") String nome, @Param("cpf") String cpf, Pageable pageRequest);
}